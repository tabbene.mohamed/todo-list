import React from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import ListTodo from './components/ListTodo';
import NotFound from './components/NotFound';
import Login from './components/Login';
import "./App.scss";


function App() {

  return (
    <div className="App">
        <Router>
            <Switch>
              <Route exact path="/">
                <Login />
              </Route>
              <Route exact path="/home">
                <Header />
                <ListTodo />
              </Route>
              <Route path="*">
                <NotFound />
              </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
