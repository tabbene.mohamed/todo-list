import React from "react";
import {Form,Col,InputGroup,Button} from 'react-bootstrap';

const AddTodo = ({
    validated,
    text,
    desc,
    handleChangeText,
    handleChangeDesc,
    handleSubmit,
}) => {    
return ( 
<>
<h3>Créer une nouvelle tâche</h3>
  <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Row class="d-flex align-items-center">
          <Form.Group as={Col} md="5">
            <Form.Label>Titre Todo</Form.Label>
            <InputGroup hasValidation>
              <Form.Control
            onChange={handleChangeText}
            value={text}
                required
              />
              <Form.Control.Feedback type="invalid">
                Please choose a Todo Name.
              </Form.Control.Feedback>
            </InputGroup>
          </Form.Group>
          <Form.Group as={Col} md="5">
            <Form.Label>Description Todo</Form.Label>
            <Form.Control 
            onChange={handleChangeDesc}
            value={desc} 
            required />
            <Form.Control.Feedback type="invalid">
              Please choose a Todo Description.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} md="2">
          <Button type="submit">Add Todo</Button>
          </Form.Group>
          </Form.Row>
      </Form> 
      </>    
    );
};


export default AddTodo;
