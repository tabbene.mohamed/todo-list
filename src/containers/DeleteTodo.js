import React,{useState} from "react";
import {Button} from 'react-bootstrap';
import SweetAlert from 'react-bootstrap-sweetalert';


const DeleteTodo = ({
    id,
    text,
    removeTodo
}) => {  
    const [alert, setAlert] = useState(false);
    const hideAlert = () => {
        setAlert(false);
      };
      const showAlert = () => {
        setAlert(true);
      };  
return ( 
<>
<Button className="delete-btn" variant="link" onClick={showAlert}>Supprimer</Button>
<SweetAlert
        show={alert}
        danger
        showCancel
        confirmBtnText="Confirme"
        cancelBtnText="Cancel"
        confirmBtnBsStyle="danger"
        title={`Vous-avez sure du delete ${text} ?`}
        onConfirm={() => removeTodo(id)}
        onCancel={() => hideAlert()}
        focusCancelBtn
      ></SweetAlert>
      </>    
    );
};

export default DeleteTodo;