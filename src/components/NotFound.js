import React from 'react';
import { useLocation } from 'react-router-dom';

function NotFound() {
  let location = useLocation();
  return (
    <h2
      style={{
        paddingTop: '300px',
        paddingBottom: '400px',
        color: '#000',
        textAlign: 'center',
      }}
    >
      Page Not Found {location.pathname}
    </h2>
  );
}

export default NotFound;