import React,{useState} from "react";
import {Container,ListGroup,Badge} from 'react-bootstrap';
import AddTodo from '../containers/AddTodo';
import DeleteTodo from '../containers/DeleteTodo';

const ListTodo = () => {
    const [validated, setValidated] = useState(false);
    const [text, setText] = useState("");
    const [desc, setDesc] = useState("");
    const [todos, setTodos] = useState([]);
  
    const handleChangeText = e => {
      setText(e.target.value);
    };
    const handleChangeDesc = e => {
      setDesc(e.target.value);
    };
  
    const addTodo = () => {
      setTodos([
        ...todos,
        {
          id: todos.length + 1,
          text: text,
          desc: desc,
          completed: false
        }
      ]);
    };
  
    const handleSubmit = (event) => {
      event.preventDefault();
      if (text !== "" && desc !== "") {
        addTodo();
        setText("");
        setDesc("");
        setValidated(false);
      }else{setValidated(true)}
      
    };
  
    const removeTodo = todoId => {
      const updatedTodos = todos.filter(todo => todo.id !== todoId);
      setTodos(updatedTodos);
    };
  
    const toggleTodo = todoId => {
      const updatedTodos = todos.map(todo => {
        return todo.id === todoId
          ? { ...todo, completed: !todo.completed }
          : todo;
      });
      setTodos(updatedTodos);
    };
  
    return ( 
    <Container>
      <AddTodo 
      validated={validated}
      text={text}
      desc={desc} 
      handleChangeText={handleChangeText}
      handleChangeDesc={handleChangeDesc}
      handleSubmit={handleSubmit} 
      />
      <hr />
      <h3>Liste des tâches</h3>
      <ListGroup>
      { todos.length >0 ? todos.map(todo => (
            <ListGroup.Item key={todo.id}>
            <div className="float-left text-desc">
                        <span onClick={() => toggleTodo(todo.id)}>
                          {todo.text}{': '}<small>{todo.desc}</small>
                        </span>{' - '}
                        <DeleteTodo 
                        id={todo.id} 
                        removeTodo={removeTodo}
                        text={todo.text} 
                        />
                        </div>
                        <div className="float-right">
                        {todo.completed ? 
                        <Badge variant="success">Complétée</Badge> 
                        : 
                        <Badge variant="danger">Non complétée</Badge>}
                        </div>                      
            </ListGroup.Item>    
            ))
          :
  <div class="alert alert-warning" role="alert">
   Liste est vide !!!
  </div>
          }
  </ListGroup>
</Container>
    );
};


export default ListTodo;
