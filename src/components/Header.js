import React from 'react';
import { Nav,Navbar,Button } from 'react-bootstrap';
import { Link,useHistory } from 'react-router-dom';


function Header() {

const history = useHistory();

  const logOut = () => {
    history.push('/');
    localStorage.removeItem('user');
  };


  return (
    <header className="header d-flex justify-content-between align-items-center bg-light">
          <Navbar bg="light" variant="light">
           <Navbar.Brand >Todo List</Navbar.Brand>
            <Nav>
              <Nav.Item>
                <Link to={'/home'}>
                  Home
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link to={'/home'}>
                Tâche
                </Link>
              </Nav.Item>
              <Nav.Item>
              <Button variant="link" onClick={logOut}>Déconnecte</Button>
              </Nav.Item>
            </Nav>
            </Navbar>
            <small className="float-right">{localStorage.getItem('user')}</small>
            </header>
  );
}

export default Header;
