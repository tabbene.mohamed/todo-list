import React, { useState, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';

const required = (value) => {
  if (!value) {
    return <div className="text-danger">This field is required!</div>;
  }
};
const email = (value) => {
  if (!/^.+@.+\..+$/.test(value) === true) {
    return <div className="text-danger">This field is required email!</div>;
  }
};

const Login = () => {
  const form = useRef();
  const checkBtn = useRef();
  const history = useHistory();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };


  const handleLogin = (e) => {
    e.preventDefault();
    form.current.validateAll();

    if(username==="test@test.com" && password==="test") 
    {
    history.push('/home')
    localStorage.setItem('user', username)
  }else{alert("verifier login ou mot passe !!!")}
  };

  return (
    <div className="login">
      <div className="container">
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-6">
            <div className="card card-container">
              <img
                src="assets/img/default_avatar.png"
                alt="profile-img"
                className="profile-img-card"
              />

              <Form onSubmit={handleLogin} ref={form}>
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="username"
                    value={username}
                    onChange={onChangeUsername}
                    validations={[required, email]}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="password"
                    value={password}
                    onChange={onChangePassword}
                    validations={[required]}
                  />
                </div>

                <div className="form-group">
                  <button className="btn btn-primary btn-block">
                    <span>Login</span>
                  </button>
                </div>
                <CheckButton style={{ display: 'none' }} ref={checkBtn} />
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
